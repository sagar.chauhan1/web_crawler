import sqlite3
import requests
from bs4 import BeautifulSoup

class CRAWLER:
	def connect_db(self):
		mydb = sqlite3.connect('Hashtags.db')
		return mydb
	
	def table_exist(self,mydb,table_name):
		cursor = mydb.cursor()
		cursor.execute('SELECT name from sqlite_master where type = "table"')
		rows = cursor.fetchall()
		for row in rows:
			return table_name in row

	def create_table(self,mydb,table_name):
		cursor = mydb.cursor()
		cursor.execute("CREATE TABLE "+table_name+" (status INTEGER, url VARCHAR, content VARCHAR );")

	def get_urls(self,i,WebUrls):
		if len(WebUrls) <=100:
			url = WebUrls[i]
			links = []
			#print(url)
			i += 1
			code = requests.get(url)
			plain = code.text
			s = BeautifulSoup(plain, "html.parser")
			for link in s.findAll('a'):
				if link.get('href') not in WebUrls:
					l = link.get('href')
					#print(l)
					if l!= None and l[0] == '/' and l != '/':
						#print(l)
						links.append(WebUrls[0]+l)
						print(links)
			WebUrls = WebUrls + links
			return self.get_urls(i,WebUrls)
		else:
			return WebUrls

	def get_content(self,WebUrls):
		contents = []
		status = []
		for url in WebUrls:
			print(url)
			code = requests.get(url)
			status.append(code)
			print(status)
			plain = code.text
			contents.append(plain)
		return contents, status

	def insert_in_db(self,mydb,table_name,links,contents,status):
		cursor = mydb.cursor()
		for i in range(len(links)):
			cursor.execute("INSERT INTO ALFA (status, url, content) VALUES (?,?,?)",(status[i],links[i],contents[i]))
		cursor.execute("SELECT id, url from "+table_name)
		mydb.commit()
		
	def web(self,WebUrls):
		links = self.get_urls(0,WebUrls)
		contents, status = self.get_content(links)
		return links, contents,status

	def get_data(self,mydb,table_name):
		cursor = mydb.cursor()
		cursor.execute("SELECT * from "+table_name)
		rows = cursor.fetchall()
		return rows