import crawler1 as cr

def retrieve(obj,mydb,table_name):
	results = obj.get_data(mydb,table_name)
	for result in results:
		print(result)

def add_data(obj,mydb,table_name):
	if obj.table_exist(mydb, table_name) is False:
		obj.create_table(mydb,table_name)
	urls = ['https://unsplash.com']
	links, contents, status = obj.web(urls)
	obj.insert_in_db(mydb,table_name,links,contents,status)
	obj.get_data(mydb,table_name)

def main():
	ch = input("1:) For adding webpages\n 2:) For retrieving Data \n ======>")
	obj = cr.CRAWLER()
	mydb = obj.connect_db()
	table_name = "ALFA"
	if ch == 1:
		add_data(obj,mydb,table_name)
	elif ch == 2:
		retrieve(obj,mydb,table_name)
	else:
		main()
	

if __name__ == '__main__':
	main()